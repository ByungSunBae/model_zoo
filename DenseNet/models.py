import tensorflow as tf
from tensorflow import layers
from tensorflow.contrib.layers import l2_regularizer

tf_layers = {
    "Conv2D": lambda filters, kernel_size, strides, name: layers.Conv2D(activation=None, 
                                                                        filters=filters, 
                                                                        kernel_size=kernel_size,
                                                                        strides=strides, 
                                                                        padding='same', 
                                                                        kernel_initializer=tf.variance_scaling_initializer,
                                                                        kernel_regularizer=l2_regularizer(1e-4),
                                                                        name=name),
    "Relu": tf.nn.relu,
    "Concat": tf.concat,
    "Softmax": tf.nn.softmax,
    "MaxPool2d": layers.MaxPooling2D,
    "AvgPool2d": layers.AveragePooling2D,
    "Flatten": lambda name: layers.Flatten(name=name),
    "Dense": lambda units, name: layers.Dense(units=units, name=name, activation=None, kernel_initializer=tf.variance_scaling_initializer),
    "Dropout": lambda keep_rate, name: layers.Dropout(rate=(1. - keep_rate), name=name),
    "BatchNorm": layers.batch_normalization
}

def dense_block(inputs, filters, scope, num_layers, is_training):
    """Block for densenet.
    """
    with tf.variable_scope(scope):
        b_cv11 = tf_layers["Conv2D"](int(4*filters), (3,3), 1, "cv1X1_0")(inputs)
        b_cv11 = tf_layers["BatchNorm"](b_cv11, training = is_training, name = "bn1X1_0")
        b_out0 = tf_layers["Relu"](b_cv11, "out0_0")
        
        b_cv33 = tf_layers["Conv2D"](filters, (3,3), 1, "cv3X3_0")(b_out0)
        b_cv33 = tf_layers["BatchNorm"](b_cv33, training = is_training, name = "bn3X3_0")
        b_out1 = tf_layers["Relu"](b_cv33, "out0_1")
        b_cc = tf_layers["Concat"]([inputs, b_out1], -1, "cc0")

        for i in range(num_layers - 2):
            b_cv11 = tf_layers["Conv2D"](int(4*filters), (1,1), 1, "cv1X1_{}".format(i+1))(b_cc)
            b_cv11 = tf_layers["BatchNorm"](b_cv11, training = is_training, name = "bn1X1_{}".format(i+1))
            b_out0 = tf_layers["Relu"](b_cv11, "out{}_0".format(i+1))
            b_cv33 = tf_layers["Conv2D"](filters, (3,3), 1, "cv3X3{}".format(i+1))(b_out0)
            b_cv33 = tf_layers["BatchNorm"](b_cv33, training = is_training, name = "bn3X3_{}".format(i+1))
            b_out1 = tf_layers["Relu"](b_cv33, "out{}_1".format(i+1))
            b_cc = tf_layers["Concat"]([b_cc, b_out1], -1, "cc{}".format(i+1))

    return b_cc


def build_densenet(input_pl, 
                   num_classes,
                   is_training_pl,
                   conv_filter=16,
                   number_block=4,
                   num_layers_in_block=[6, 12, 24, 16], 
                   block_filter=12, 
                   model_scope="DenseNet",
                   reuse=tf.AUTO_REUSE):
    """Build Densenet from https://arxiv.org/pdf/1608.06993.pdf.
    
    Args:
      input_pl: tf.placeholder. a 4-D tensor of size [batch_size, height, width, 3].
      num_classes: int. number of classes to classify data.
      is_training_pl: tf.placeholder. True or False.
      conv_filter: int. the positions of conv filter are first and next block.
      number_block: int. number of blocks.
      num_layers_in_block: list. list of number of layer in each block.
      block_filter: int. filter of blocks.
      model_scope: str. scope of model.
      resue: tf.AUTO_REUSE.
    
    Returns:
      logits: the output of the logits layer.
      end_points: the set of end_points from the densenet.
      
    Raises:
      AssertionError: number_blok must equal to len(num_layers_in_block).
    """
    end_points = {}
    with tf.variable_scope(model_scope, reuse=reuse):
        conv = tf_layers["Conv2D"](conv_filter, (7, 7), 2, "conv0")(input_pl)
        pool = tf_layers["MaxPool2d"]((2,2), 2, name = 'pool0')(conv)

        assert number_block == len(num_layers_in_block)

        for i in range(number_block):
            if i != (number_block - 1):
                block_name = "block{}".format(i)
                conv_name = "conv{}".format(i+1)
                pool_name = "pool{}".format(i+1)
                
                block = dense_block(pool, block_filter, block_name, num_layers_in_block[i], is_training_pl)
                conv = tf_layers["Conv2D"](block.shape.as_list()[-1] // 2, (1, 1), 1, conv_name)(block)
                pool = tf_layers["AvgPool2d"]((2,2), 2, name = pool_name)(conv)
                
                end_points[block_name] = block
                end_points[conv_name] = conv
                end_points[pool_name] = pool
                
            elif i == (number_block - 1):
                block_name = "block{}".format(i)
                block = dense_block(pool, block_filter, block_name, num_layers_in_block[i], is_training_pl)
                end_points[block_name] = block

        _, bh, bw, _ = block.shape.as_list()

        #linear = tf_layers["Flatten"]("global_avgpool")(tf_layers["AvgPool2d"]((bh, bw), 1)(block))
        linear = tf.squeeze(tf.reduce_mean(block, [1, 2], True), [1, 2], name = "global_avgpool")
        end_points["global_avgpool"] = linear
        
        logits = tf_layers["Dense"](num_classes, "logits")(linear)
        end_points["logits"] = logits
        
        predictions = tf_layers["Softmax"](logits, name = "predictions")
        end_points["predictions"] = predictions
    return logits, end_points